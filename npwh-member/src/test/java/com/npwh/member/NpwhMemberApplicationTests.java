package com.npwh.member;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;

import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.temporal.TemporalField;
import java.util.*;

import static java.time.temporal.ChronoField.MILLI_OF_SECOND;

@SpringBootTest
class NpwhMemberApplicationTests {

    @Test
    void contextLoads() {

		/*Map map = new HashMap();

		try {
			ClassPathResource classPathResource = new ClassPathResource("/json/pca-code.json");
			String str = IOUtils.toString(new InputStreamReader(classPathResource.getInputStream(), "UTF-8"));
			map = JSONObject.parseObject(str, HashMap.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println(map.toString());*/

        // System.out.println(getDaysBetwwen(7));

        LocalDate today = LocalDate.now();
        Instant now = Instant.now();
        System.out.println(now.getEpochSecond());
    }

    private static List<String> getDaysBetwwen(int days) { //最近几天日期
        List<String> dayss = new ArrayList<>();
        Calendar start = Calendar.getInstance();
        start.setTime(getDateAdd(days));
        long startTIme = start.getTimeInMillis();
        Calendar end = Calendar.getInstance();
        end.setTime(new Date());
        long endTime = end.getTimeInMillis();
        long oneDay = 1000 * 60 * 60 * 24L;
        long time = startTIme;
        while (time <= endTime) {
            Date d = new Date(time);
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            // System.out.println(df.format(d));
            dayss.add(df.format(d));
            time += oneDay;
        }
        return dayss;
    }

    private static Date getDateAdd(int days) {
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DAY_OF_MONTH, -days);
        return c.getTime();
    }

}
