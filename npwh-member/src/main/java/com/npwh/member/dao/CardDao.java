package com.npwh.member.dao;

import com.npwh.member.entity.CardEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.npwh.member.vo.CardTotalVo;
import com.npwh.member.vo.CardVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 会员卡表
 *
 * @author Kings
 * @email cqzhongxc@gmail.com
 * @date 2020-09-23 10:38:08
 */
@Mapper
public interface CardDao extends BaseMapper<CardEntity> {
    CardVo selectByCardNumber(@Param("cardNumber") String cardNumber);

    List<CardTotalVo> cardTotalMap(@Param("typeId") Integer typeId);
}
