package com.npwh.member.dao;

import com.npwh.member.entity.CnAreaEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 *  县级（区县）
 * 
 * @author Kings
 * @email cqzhongxc@gmail.com
 * @date 2020-09-23 16:55:57
 */
@Mapper
public interface CnAreaDao extends BaseMapper<CnAreaEntity> {
	
}
