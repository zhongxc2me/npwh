package com.npwh.member.dao;

import com.npwh.member.entity.DictionaryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 数据字典表 
 * 
 * @author Kings
 * @email cqzhongxc@gmail.com
 * @date 2020-09-23 10:38:08
 */
@Mapper
public interface DictionaryDao extends BaseMapper<DictionaryEntity> {
	
}
