package com.npwh.member.dao;

import com.npwh.member.entity.CardtypeEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员卡类型表 
 * 
 * @author Kings
 * @email cqzhongxc@gmail.com
 * @date 2020-09-23 10:38:08
 */
@Mapper
public interface CardtypeDao extends BaseMapper<CardtypeEntity> {
	
}
