package com.npwh.member.dao;

import com.npwh.member.entity.AttendanceEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.npwh.member.vo.AtterdanceVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 会员考勤记录表
 *
 * @author Kings
 * @email cqzhongxc@gmail.com
 * @date 2020-10-10 10:29:37
 */
@Mapper
public interface AttendanceDao extends BaseMapper<AttendanceEntity> {
    AtterdanceVo getMemberByNumber(@Param("cardNumber") String cardNumber);
}
