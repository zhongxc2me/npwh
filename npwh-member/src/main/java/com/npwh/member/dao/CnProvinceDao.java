package com.npwh.member.dao;

import com.npwh.member.entity.CnProvinceEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 省级（省份直辖市自治区）
 * 
 * @author Kings
 * @email cqzhongxc@gmail.com
 * @date 2020-09-23 16:55:58
 */
@Mapper
public interface CnProvinceDao extends BaseMapper<CnProvinceEntity> {
	
}
