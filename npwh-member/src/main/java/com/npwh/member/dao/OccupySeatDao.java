package com.npwh.member.dao;

import com.npwh.member.entity.OccupySeatEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 剧场座位号发放
 * 
 * @author Kings
 * @email cqzhongxc@gmail.com
 * @date 2020-10-30 22:31:07
 */
@Mapper
public interface OccupySeatDao extends BaseMapper<OccupySeatEntity> {
	
}
