package com.npwh.member.vo;

import lombok.Data;

@Data
public class CardTotalVo {

    private String day;
    private Integer total;

}
