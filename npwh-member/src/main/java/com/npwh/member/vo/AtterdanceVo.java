package com.npwh.member.vo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class AtterdanceVo {
    /**
     * 会员资料编号
     */
    private Integer memberId;
    /**
     * 会员卡编号
     */
    private Integer cardId;
    /**
     * 会员卡号
     */
    private String cardNumber;
    /**
     * 姓名
     */
    private String name;
    /**
     * 会员卡类型编号
     */
    private Integer cardTypeId;
    /**
     * 会员卡类型
     */
    private String cardTypeName;
    /**
     * 卡内余额
     */
    private BigDecimal balance;
    /**
     * 剩余可用次数
     */
    private Integer usage;
    /**
     * 开卡时间
     */
    private String createdTime;
    /**
     * 有效日期
     */
    private Long expDate;
    /**
     * 累计考勤次数
     */
    private Integer totalAttda;
    /**
     * 上次来访日期
     */
    private String lastVisit;
    /**
     * 联系电话
     */
    private String telphone;
    /**
     * 头像
     */
    private String avatar;

}
