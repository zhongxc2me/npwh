package com.npwh.member.vo;

import lombok.Data;

@Data
public class MemberVo {
    private Integer id;
    private String name;
    private Integer age;
    private Integer sex;
    private String telphone;
    private Long birthday;
    private String address;
    private Integer hometown;
    private Integer education;
    private String hobby;
    private String health;
    private Integer marriage;
    private String provincecode;
    private String provincename;
    private String citycode;
    private String cityname;
    private String areacode;
    private String areaname;
    private Long createdTime;

    private Integer cardTypeId;
    private String cardTypeName;
    private Long expDate;
    private String cardNumber;
    private Long createdTimeByCard;
    private int usage;
    private String seat;
}
