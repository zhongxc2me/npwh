package com.npwh.member.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.npwh.common.utils.PageUtils;
import com.npwh.common.utils.Query;
import com.npwh.member.dao.MemberDao;
import com.npwh.member.entity.CardEntity;
import com.npwh.member.entity.MemberEntity;
import com.npwh.member.service.CardService;
import com.npwh.member.service.MemberService;
import com.npwh.member.vo.MemberVo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Service("memberService")
public class MemberServiceImpl extends ServiceImpl<MemberDao, MemberEntity> implements MemberService {

    MemberDao memberDao;

    @Autowired
    public void setMemberDao(MemberDao memberDao) {
        this.memberDao = memberDao;
    }

    CardService cardService;

    @Autowired
    public void setCardService(CardService cardService) {
        this.cardService = cardService;
    }

    @Value("${upload.access.path}")
    private String domainName;

    @Override
    public Page<MemberVo> queryPageList(Map<String, Object> params) throws Exception {

        QueryWrapper<MemberVo> wrapper = new QueryWrapper<>();

        long limit = Long.parseLong(params.get("limit") + "");
        long page = Long.parseLong(params.get("page") + "");

        String name = (String) params.get("name");  // 姓名
        String address = (String) params.get("address");    // 居住地址
        String hobby = (String) params.get("hobby");    // 兴趣爱好
        String health = (String) params.get("health");  // 健康状况

        String sex = (String) params.get("sex");    // 性别
        String birthday = (String) params.get("birthday");  // 生日
        String provincecode = (String) params.get("provincecode");  // 省份
        String citycode = (String) params.get("citycode");  // 城市
        String areacode = (String) params.get("areacode");  // 区县
        String cardTypeId = (String) params.get("cardTypeId"); // 会员卡类型
        String education = (String) params.get("education");    // 学历
        String marriage = (String) params.get("marriage");  // 婚姻状况
        String hometown = (String) params.get("hometown"); // 籍贯
        String expdate = (String) params.get("expdate"); // 会员到期时间
        String orderby = "";
        if ("cardNumberDesc".equals(params.get("orderby"))) {
            orderby = "b.card_number";
        }

        wrapper.eq("status", 1)
                .like(StringUtils.isNotBlank(name), "a.name", name)  // 姓名
                .like(StringUtils.isNotBlank(address), "a.address", address)  // 地址
                .like(StringUtils.isNotBlank(hobby), "a.hobby", hobby)    // 兴趣爱好
                .like(StringUtils.isNotBlank(health), "a.health", health)    // 健康状况
                .eq(StringUtils.isNotBlank(sex), "a.sex", sex)  // 性别
                .eq(StringUtils.isNotBlank(birthday), "a.birthday", birthday)   // 生日
                .eq(StringUtils.isNotBlank(provincecode), "a.provincecode", provincecode)   // 省份代码
                .eq(StringUtils.isNotBlank(citycode), "a.citycode", citycode)   // 城市代码
                .eq(StringUtils.isNotBlank(areacode), "a.areacode", areacode)   // 区县代码
                .eq(StringUtils.isNotBlank(cardTypeId), "b.card_type_id", cardTypeId)   // 会员卡类型
                .eq(StringUtils.isNotBlank(education), "a.education", education)    // 学历
                .eq(StringUtils.isNotBlank(marriage), "a.marriage", marriage)    // 婚姻状况
                .eq(StringUtils.isNotBlank(hometown), "a.hometown", hometown)    // 籍贯
                .eq(StringUtils.isNotBlank(expdate), "b.exp_date", expdate); // 会员到期时间
        if (StringUtils.isNotBlank(orderby)) {
            wrapper.orderByDesc(orderby);
        } else {
            wrapper.orderByDesc("id");
        }

        Page<MemberVo> objectPage = new Page<>(page, limit);

        return memberDao.queryPageList(objectPage, wrapper);
    }

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String name = (String) params.get("name");  // 姓名
        String sex = (String) params.get("sex");    // 星币
        String birthday = (String) params.get("birthday");  // 生日
        String provincecode = (String) params.get("provincecode");  // 省份
        String citycode = (String) params.get("citycode");  // 城市
        String areacode = (String) params.get("areacode");  // 区县
        String cardTypeId = (String) params.get("cardTypeId"); // 会员卡类型
        String address = (String) params.get("address");    // 居住地址


        IPage<MemberEntity> page = this.page(
                new Query<MemberEntity>().getPage(params),
                new QueryWrapper<MemberEntity>()
                        .like(StringUtils.isNotBlank(name), "name", name)
                        .eq(StringUtils.isNotBlank(sex), "sex", sex)
                        .eq(StringUtils.isNotBlank(birthday), "birthday", birthday)
                        .eq(StringUtils.isNotBlank(provincecode), "provincecode", provincecode)
                        .eq(StringUtils.isNotBlank(citycode), "citycode", citycode)
                        .eq(StringUtils.isNotBlank(areacode), "areacode", areacode)
                        .eq(StringUtils.isNotBlank(address), "address", address)
        );
        return new PageUtils(page);
    }

    @Override
    public List<MemberEntity> selectByName(String name) {
        List<MemberEntity> list = memberDao.selectList(new QueryWrapper<MemberEntity>().like("name", name));
        /*List<CardEntity> collect = list.stream().map(item -> {
            return cardService.getById(item.getId());
        }).collect(Collectors.toList());*/
        List<MemberEntity> resultList = new ArrayList<>();
        list.forEach(item -> {
            CardEntity entity = cardService.getOne(
                    new QueryWrapper<CardEntity>()
                            .eq("member_id", item.getId())
            );
            if (entity == null) {
                resultList.add(item);
            }
        });
        resultList.forEach(item -> item.setName(item.getName() + " (" + item.getTelphone() + ")"));
        return resultList;
    }

    @Override
    public List<MemberEntity> selectByIdCard(String idCard) {
        List<MemberEntity> list = memberDao.selectList(new QueryWrapper<MemberEntity>().eq("idcard", idCard));
        list.forEach(item -> item.setAvatar(domainName + item.getAvatar()));
        return list;
    }


}