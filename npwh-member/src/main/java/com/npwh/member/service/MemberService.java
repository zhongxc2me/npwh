package com.npwh.member.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.npwh.common.utils.PageUtils;
import com.npwh.member.entity.MemberEntity;
import com.npwh.member.vo.MemberVo;

import java.util.List;
import java.util.Map;

/**
 * 会员基础资料表
 *
 * @author Kings
 * @email cqzhongxc@gmail.com
 * @date 2020-09-23 10:38:08
 */
public interface MemberService extends IService<MemberEntity> {

    Page<MemberVo> queryPageList(Map<String, Object> params) throws Exception;

    PageUtils queryPage(Map<String, Object> params);

    List<MemberEntity> selectByName(String name);

    List<MemberEntity> selectByIdCard(String idCard);


}

