package com.npwh.member.service.impl;

import com.npwh.member.dao.CardDao;
import com.npwh.member.entity.CardEntity;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.npwh.common.utils.PageUtils;
import com.npwh.common.utils.Query;

import com.npwh.member.dao.OccupySeatDao;
import com.npwh.member.entity.OccupySeatEntity;
import com.npwh.member.service.OccupySeatService;


@Service("occupySeatService")
public class OccupySeatServiceImpl extends ServiceImpl<OccupySeatDao, OccupySeatEntity> implements OccupySeatService {

    @Autowired
    CardDao cardDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<OccupySeatEntity> page = this.page(
                new Query<OccupySeatEntity>().getPage(params),
                new QueryWrapper<OccupySeatEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public int getSeatNo(String dramaId, String cardNumber) {

        // 1. 查询会员是否已有指定到座位号
        CardEntity cardEntity = cardDao.selectOne(
                new QueryWrapper<CardEntity>()
                        .eq("card_number", cardNumber)
        );
        Integer seat = cardEntity.getSeat();
        // 判断是否已指定座位号
        if (seat != null && seat > 0) {
            return seat;
        }

        // 2. 按剧场编号查询已发放了到号码
        List<OccupySeatEntity> selectList = this.baseMapper.selectList(
                new QueryWrapper<OccupySeatEntity>()
                        .eq("drama_id", dramaId)
        );

        // 查询已指定给了会员的座位号
        List<CardEntity> seatList = cardDao.selectList(new QueryWrapper<CardEntity>().isNotNull("seat"));

        List<Integer> arrayList = new ArrayList<Integer>();

        // 已被选择的座位号加入数组
        selectList.forEach(item -> {
            arrayList.add(item.getSeatNumber());
        });

        // 已发放的座位号加入数组
        seatList.forEach(item -> {
            arrayList.add(item.getSeat());
        });

        Integer[] selectdSeat = new Integer[arrayList.size()];
        for (int i = 0; i < arrayList.size(); i++) {
            selectdSeat[i] = arrayList.get(i);
        }

        // 生成 60 - 150 数组
        // int[] unseat = new int[150 - 61];
        int idx = 0;
        Integer[] unseat = new Integer[150];
        for (int i = 1; i <= 150; i++) {
            unseat[idx] = i;
            idx++;
        }

        // 求两数组的差集
        Integer[] c = getC(unseat, selectdSeat);
        if (c.length > 0) {
            // 数组排序
            Arrays.sort(c);
            if (c[0] < 61) {
                List<Integer> list = new ArrayList<>();
                for (int i = 0; i < c.length; i++) {
                    if (c[i] > 60) {
                        list.add(c[i]);
                    }
                }
                return list.get(0);
            }
            return c[0];
        }
        // 没有交集，即没有了座位号
        return 0;
    }

    /**
     * 求差集
     *
     * @param m
     * @param n
     * @return
     */
    private static Integer[] getC(Integer[] m, Integer[] n) {
        // 将较长的数组转换为set
        Set<Integer> set = new HashSet<Integer>(Arrays.asList(m.length > n.length ? m : n));

        // 遍历较短的数组，实现最少循环
        for (Integer i : m.length > n.length ? n : m) {
            // 如果集合里有相同的就删掉，如果没有就将值添加到集合
            if (set.contains(i)) {
                set.remove(i);
            } else {
                set.add(i);
            }
        }

        Integer[] arr = {};
        return set.toArray(arr);
    }

}