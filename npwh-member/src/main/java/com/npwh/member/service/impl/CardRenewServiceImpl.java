package com.npwh.member.service.impl;

import com.npwh.member.dao.CardDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.npwh.common.utils.PageUtils;
import com.npwh.common.utils.Query;

import com.npwh.member.dao.CardRenewDao;
import com.npwh.member.entity.CardRenewEntity;
import com.npwh.member.service.CardRenewService;


@Service("cardRenewService")
public class CardRenewServiceImpl extends ServiceImpl<CardRenewDao, CardRenewEntity> implements CardRenewService {

    @Autowired
    CardDao cardDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        int cardId = Integer.parseInt(params.get("cardId") + "");
        IPage<CardRenewEntity> page = this.page(
                new Query<CardRenewEntity>().getPage(params),
                new QueryWrapper<CardRenewEntity>()
                        .eq(cardId > 0, "card_id", cardId)
                        .orderByDesc("created_time")
        );

        return new PageUtils(page);
    }

}