package com.npwh.member.service.impl;

import com.npwh.member.vo.AtterdanceVo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.npwh.common.utils.PageUtils;
import com.npwh.common.utils.Query;

import com.npwh.member.dao.AttendanceDao;
import com.npwh.member.entity.AttendanceEntity;
import com.npwh.member.service.AttendanceService;


@Service("attendanceService")
public class AttendanceServiceImpl extends ServiceImpl<AttendanceDao, AttendanceEntity> implements AttendanceService {

    @Value("${upload.access.path}")
    private String domainName;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        int memberId = 0;
        System.out.println(params.toString());
        if (StringUtils.isNotEmpty(params.get("memberId") + "")) {
            try {
                memberId = Integer.parseInt(params.get("memberId") + "");
            } catch (NumberFormatException e) {
                memberId = 0;
            }
        }

        String cardNumber = (String) params.get("cardNumber");
        String memberName = (String) params.get("memberName");

        IPage<AttendanceEntity> page = this.page(
                new Query<AttendanceEntity>().getPage(params),
                new QueryWrapper<AttendanceEntity>()
                        .eq(memberId > 0, "member_id", memberId)
                        .eq(StringUtils.isNotBlank(cardNumber), "card_number", cardNumber)
                        .like(StringUtils.isNotBlank(memberName), "member_name", memberName)
                        .orderByDesc("id")
        );
        return new PageUtils(page);
    }

    @Override
    public AtterdanceVo getMemberByNumber(String cardNumber) {
        AtterdanceVo vo = baseMapper.getMemberByNumber(cardNumber);
        if (vo != null) {
            vo.setAvatar(domainName + vo.getAvatar());
        }
        return vo;
    }

}