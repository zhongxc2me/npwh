package com.npwh.member.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.npwh.common.utils.PageUtils;
import com.npwh.common.utils.Query;

import com.npwh.member.dao.CnProvinceDao;
import com.npwh.member.entity.CnProvinceEntity;
import com.npwh.member.service.CnProvinceService;


@Service("cnProvinceService")
public class CnProvinceServiceImpl extends ServiceImpl<CnProvinceDao, CnProvinceEntity> implements CnProvinceService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        params.put("limit", "100");
        IPage<CnProvinceEntity> page = this.page(
                new Query<CnProvinceEntity>().getPage(params),
                new QueryWrapper<CnProvinceEntity>()
        );

        return new PageUtils(page);
    }

}