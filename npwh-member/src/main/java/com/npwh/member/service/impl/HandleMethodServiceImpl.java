package com.npwh.member.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.npwh.common.utils.PageUtils;
import com.npwh.common.utils.Query;

import com.npwh.member.dao.HandleMethodDao;
import com.npwh.member.entity.HandleMethodEntity;
import com.npwh.member.service.HandleMethodService;


@Service("handleMethodService")
public class HandleMethodServiceImpl extends ServiceImpl<HandleMethodDao, HandleMethodEntity> implements HandleMethodService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<HandleMethodEntity> page = this.page(
                new Query<HandleMethodEntity>().getPage(params),
                new QueryWrapper<HandleMethodEntity>()
        );

        return new PageUtils(page);
    }

}