package com.npwh.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.npwh.common.utils.PageUtils;
import com.npwh.member.entity.DramaEntity;

import java.util.Map;

/**
 * 
 *
 * @author Kings
 * @email cqzhongxc@gmail.com
 * @date 2020-10-28 14:39:09
 */
public interface DramaService extends IService<DramaEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

