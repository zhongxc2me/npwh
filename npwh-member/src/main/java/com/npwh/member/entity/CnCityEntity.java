package com.npwh.member.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 地级（城市）
 * 
 * @author Kings
 * @email cqzhongxc@gmail.com
 * @date 2020-09-23 16:55:58
 */
@Data
@TableName("cn_city")
public class CnCityEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 城市主键编号
	 */
	@TableId
	private Integer cid;
	/**
	 * 城市代码
	 */
	private String code;
	/**
	 * 城市名称
	 */
	private String name;
	/**
	 * 省份代码
	 */
	private String provinceCode;
	/**
	 * 等级划分，1：一线城市；2：二线城市；3：三线城市；4：四线城市；5：五线城市
	 */
	private Integer level;

}
