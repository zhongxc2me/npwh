package com.npwh.member.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 *  县级（区县）
 * 
 * @author Kings
 * @email cqzhongxc@gmail.com
 * @date 2020-09-23 16:55:57
 */
@Data
@TableName("cn_area")
public class CnAreaEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 区县主键编号
	 */
	@TableId
	private Integer areaid;
	/**
	 * 区县代码
	 */
	private String code;
	/**
	 * 区县名称
	 */
	private String name;
	/**
	 * 城市代码
	 */
	private String cityCode;
	/**
	 * 省份代码
	 */
	private String provinceCode;
	/**
	 * 是否撤销 0：撤销；1：正常
	 */
	private Integer cancel;

}
