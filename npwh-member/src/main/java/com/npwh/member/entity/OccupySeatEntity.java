package com.npwh.member.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 剧场座位号发放
 * 
 * @author Kings
 * @email cqzhongxc@gmail.com
 * @date 2020-10-30 22:31:07
 */
@Data
@TableName("ums_occupy_seat")
public class OccupySeatEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 座位编号
	 */
	@TableId
	private Integer id;

	/**
	 * 会员编号
	 */
	private Integer cardId;

	/**
	 * 会员资料编号
	 */
	private Integer memberId;
	/**
	 * 戏剧编号
	 */
	private Integer dramaId;
	/**
	 * 座位号码
	 */
	private Integer seatNumber;
	/**
	 * 创建时间
	 */
	private Long createdTime;

}
