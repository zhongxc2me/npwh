package com.npwh.member.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author Kings
 * @email cqzhongxc@gmail.com
 * @date 2020-10-28 14:39:09
 */
@Data
@TableName("ums_drama")
public class DramaEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 戏剧编号
	 */
	@TableId
	private Integer id;
	/**
	 * 戏剧名称
	 */
	@TableField("`name`")
	private String name;
	/**
	 * 主演
	 */
	private String starring;
	/**
	 * 片长
	 */
	@TableField("`length`")
	private String length;

	/**
	 * 观影日期
	 */
	private Long viewDate;
	/**
	 * 入场须知
	 */
	private String notice;
	/**
	 * 备注
	 */
	private String remark;
	/**
	 * 状态，0：下线；1：正常
	 */
	@TableField("`status`")
	private Integer status;
	/**
	 * 创建人
	 */
	private String createdBy;
	/**
	 * 创建时间
	 */
	private Long createdTime;
	/**
	 * 更新人
	 */
	private String updatedBy;
	/**
	 * 更新时间
	 */
	private Long updatedTime;

}
