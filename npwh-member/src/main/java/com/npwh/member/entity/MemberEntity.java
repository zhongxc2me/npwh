package com.npwh.member.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 会员基础资料表
 * 
 * @author Kings
 * @email cqzhongxc@gmail.com
 * @date 2020-09-23 10:38:08
 */
@Data
@TableName("ums_member")
public class MemberEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 会员编号
	 */
	@TableId
	private Integer id;
	/**
	 * 姓名
	 */
	@TableField("`name`")
	private String name;
	/**
	 * 年龄
	 */
	private Integer age;
	/**
	 * 性别 1：男；0：女
	 */
	private Integer sex;
	/**
	 * 头像
	 */
	private String avatar;
	/**
	 * 联系电话
	 */
	private String telphone;
	/**
	 * 身份证号
	 */
	private String idcard;
	/**
	 * 出生日期
	 */
	private Integer birthday;
	/**
	 * 居住地址
	 */
	private String address;
	/**
	 * 籍贯 1：汉族；2：少数民族
	 */
	private Integer hometown;
	/**
	 * 籍贯 地名、祖籍
	 */
	private String hometownVal;
	/**
	 * 职业
	 */
	private String job;
	/**
	 * 学历 1：文盲；2：小学；3：初中；4：高中/技校/中专；5：大学专科以上；6：不详
	 */
	private Integer education;
	/**
	 * 有无保险 0：无；1：有；
	 */
	private Integer insurance;
	/**
	 * 兴趣爱好
	 */
	private String hobby;
	/**
	 * 健康状况
	 */
	private String health;
	/**
	 * 婚姻状况 1：未婚；2：已婚；3：丧偶；4：离婚；5：未说明的婚姻状况
	 */
	private Integer marriage;
	/**
	 * 居住情况 1：独居；2：与子女居住；3：夫妻同居；4：其他
	 */
	private Integer reside;
	/**
	 * 省份编码
	 */
	private String provincecode;
	/**
	 * 省份名称
	 */
	private String provincename;
	/**
	 * 城市编码
	 */
	private String citycode;
	/**
	 * 城市名称
	 */
	private String cityname;
	/**
	 * 区域编码
	 */
	private String areacode;
	/**
	 * 区域名称
	 */
	private String areaname;
	/**
	 * 状态 1：正常；0: 删除
	 */
	@TableField("`status`")
	private Integer status;
	/**
	 * 乐观锁
	 */
	private Integer revision;
	/**
	 * 创建人
	 */
	private String createdBy;
	/**
	 * 创建时间
	 */
	private Long createdTime;
	/**
	 * 更新人
	 */
	private String updatedBy;
	/**
	 * 更新时间
	 */
	private Long updatedTime;

}
