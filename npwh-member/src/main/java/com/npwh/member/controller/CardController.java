package com.npwh.member.controller;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

// import org.apache.shiro.authz.annotation.RequiresPermissions;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.npwh.common.utils.Constant;
import com.npwh.member.entity.*;
import com.npwh.member.service.*;
import com.npwh.member.vo.CardVo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.npwh.common.utils.PageUtils;
import com.npwh.common.utils.R;


/**
 * 会员卡表
 *
 * @author Kings
 * @email cqzhongxc@gmail.com
 * @date 2020-09-23 10:38:08
 */
@RestController
@RequestMapping("member/card")
public class CardController {

    private CardService cardService;

    @Autowired
    public void setCardService(CardService cardService) {
        this.cardService = cardService;
    }

    private CardtypeService cardtypeService;

    @Autowired
    public void setCardtypeService(CardtypeService cardtypeService) {
        this.cardtypeService = cardtypeService;
    }

    private MemberService memberService;

    @Autowired
    public void setMemberService(MemberService memberService) {
        this.memberService = memberService;
    }

    private CardRenewService cardRenewService;

    @Autowired
    public void setCardRenewService(CardRenewService cardRenewService) {
        this.cardRenewService = cardRenewService;
    }

    private DramaService dramaService;

    @Autowired
    public void setDramaService(DramaService dramaService) {
        this.dramaService = dramaService;
    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    // @RequiresPermissions("member:card:list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = cardService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    // @RequiresPermissions("member:card:info")
    public R info(@PathVariable("id") Integer id) {
        CardEntity card = cardService.getById(id);

        return R.ok().put("card", card);
    }

    @RequestMapping("/select/{column}/{val}")
    public R select(@PathVariable("column") String column,
                    @PathVariable("val") String val) {
        Map<String, Object> map = cardService.getMap(new QueryWrapper<CardEntity>().eq(column, val));
        return R.ok().put("data", map);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    // @RequiresPermissions("member:card:save")
    public R save(@RequestBody CardEntity card) {
        // 按卡号查询会员卡信息
        CardEntity cardEntity = cardService.getOne(new QueryWrapper<CardEntity>().eq("card_number", card.getCardNumber()));
        if (cardEntity != null) {
            return R.error("该会员卡已被其他人办理了");
        }

        // 一人只允许办理一张会员卡
        CardEntity hasCard = cardService.getOne(new QueryWrapper<CardEntity>().eq("member_id", card.getMemberId()));
        if (hasCard != null) {
            return R.error("您已经办理过会员卡了，请不要重复办理");
        }

        // 判断座位号是否被其他人占用
        int seat = cardService.count(new QueryWrapper<CardEntity>().eq("seat", card.getSeat()));
        if (seat > 0) {
            return R.error("该座位号已被其他人占用，请选择其他座位号");
        }

        // 获取会员卡类型
        CardtypeEntity cardtypeEntity = cardtypeService.getById(card.getCardTypeId());

        // 只有超级管理员才拥有不被限制的权限
        if (Constant.SUPER_ADMIN != card.getUserId()) {
            // 统计推荐人
            List<CardEntity> list = cardService.list(
                    new QueryWrapper<CardEntity>()
                            .eq("referrer_id", card.getMemberId())
                            .eq("card_type_code", cardtypeEntity.getRequireCode())
            );

            // 判断是否满足推荐的要求
            if (list.size() < cardtypeEntity.getRequire()) {
                return R.error("该用户不满足办理<" + cardtypeEntity.getName() + ">的要求");
            }
        }
        // System.out.println(card);
        // 查询推荐人详细信息
        if (card.getReferrerId() != null) {
            MemberEntity memberEntity = memberService.getById(card.getReferrerId());
            if (memberEntity != null) {
                // 推荐人姓名
                card.setReferrer(memberEntity.getName());
            }
        }
        // 会员卡类型代码
        card.setCardTypeCode(cardtypeEntity.getCode());
        // 办卡记录创建时间
        card.setCreatedTime(new Date().getTime() / 1000);
        // 保存会员卡
        cardService.save(card);

        // 查询新办的会员卡
        cardEntity = cardService.getOne(
                new QueryWrapper<CardEntity>()
                        .eq("card_number", card.getCardNumber())
        );

        // 记录首次默认续费记录
        CardRenewEntity renewEntity = new CardRenewEntity();

        // 当前充值可用次数
        renewEntity.setUsage(card.getUsage());
        // 上次剩余可用次数
        renewEntity.setLastusage(0);
        // 当前总的可用次数
        renewEntity.setTotalusage(card.getUsage());
        // 有效日期
        renewEntity.setExpiryDate(card.getExpDate());

        renewEntity.setCardId(cardEntity.getId());
        renewEntity.setUsage(card.getUsage());
        renewEntity.setRemark("首次办卡，默认充值记录");
        renewEntity.setCreatedTime(new Date().getTime() / 1000);
        renewEntity.setCreatedBy(card.getCreatedBy());
        cardRenewService.save(renewEntity);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    // @RequiresPermissions("member:card:update")
    public R update(@RequestBody CardEntity card) {
        cardService.updateById(card);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    // @RequiresPermissions("member:card:delete")
    public R delete(@RequestBody Integer[] ids) {
        cardService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

    /**
     * 按会员卡号查询会员信息
     *
     * @param cardNumber 会员卡号
     * @return JSONObject
     */
    @RequestMapping("/cardnumber/{cardNumber}")
    public R selectByCardNumber(@PathVariable String cardNumber) {
        CardVo cardVo = cardService.selectByCardNumber(cardNumber);
        return R.ok().put("data", cardVo);
    }

    /**
     * 按会员卡号查询会员信息
     *
     * @param cardNumber 会员卡号
     * @return JSONObject
     */
    @RequestMapping("/infos/{cardNumber}")
    public R getMemberinfo(@PathVariable String cardNumber) {
        CardVo cardVo = cardService.selectByCardNumber(cardNumber);

        List<DramaEntity> list = dramaService.list(
                new QueryWrapper<DramaEntity>()
                        .eq("status", 1)
                        .orderByAsc("view_date")
        );
        cardVo.setDramas(list);

        return R.ok().put("data", cardVo);
    }

    @RequestMapping("/total")
    public R cardTotal() {

        Map<String, Object> map = cardService.cardTotalMap();
        return R.ok().put("data", map);
    }

}
