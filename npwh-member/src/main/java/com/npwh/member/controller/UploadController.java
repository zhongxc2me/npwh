package com.npwh.member.controller;

import com.npwh.common.utils.R;
import org.apache.commons.lang.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Date;
import java.util.HashMap;

@RestController
@RequestMapping("member/upload")
public class UploadController {

    @Value("${upload.file.path}")
    private String homePath;

    @Value("${upload.access.path}")
    private String domainName;

    @Resource
    private ResourceLoader resourceLoader;

    @RequestMapping(value = "/uploadfile", method = RequestMethod.POST)
    public R uploadFile(@RequestParam("file") MultipartFile file) throws IOException {
        // 文件名
        String fileName = file.getOriginalFilename();
        // 在file文件夹中创建名为fileName的文件
        assert fileName != null;
        int split = fileName.lastIndexOf(".");
        // 文件后缀，用于判断上传的文件是否是合法的
        String suffix = fileName.substring(split + 1, fileName.length());
        // 判断问价类型，因为是图片，所以只设置三种合法的格式
        String url = "";
        String newName = "";
        String today = "";
        if ("jpg".equals(suffix) || "jpeg".equals(suffix) || "png".equals(suffix)) {
            // 正确的文件类型，保存文件
            try {
                today = DateFormatUtils.format(new Date(), "yyyyMMdd");
                /*File path = new File(ResourceUtils.getURL("classpath").getPath());
                File upload = new File(path.getAbsolutePath(), "upload/");
                if (!upload.exists()) {
                    upload.mkdirs();
                }*/
                String saveFilePath = homePath + today + "/";
                File upload = new File(saveFilePath);
                if (!upload.exists()) {
                    if (!upload.mkdirs()) {
                        return R.error(500, "文件目录创建失败");
                    }
                }
                newName = System.currentTimeMillis() + "." + suffix;
                File savedFile = new File(saveFilePath + newName);
                file.transferTo(savedFile);
                url = savedFile.getAbsolutePath();
                System.out.println("图片上传完毕，存储地址为：" + url);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        HashMap<String, String> res = new HashMap<>();
        res.put("imageUrl", domainName + today + "/" + newName);
        res.put("imageName", today + "/" + newName);
        return R.ok().put("data", res);
    }

    @RequestMapping("/{day}/{filename:.+}")
    public ResponseEntity<?> getFile(@PathVariable String day, @PathVariable String filename) {
        // String homePath = "/Users/kings/data/images/";
        return ResponseEntity.ok(resourceLoader.getResource("file:" + Paths.get(homePath + day + "/" + filename)));
    }
}
