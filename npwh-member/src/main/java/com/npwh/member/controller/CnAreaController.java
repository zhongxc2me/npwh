package com.npwh.member.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

// import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.npwh.member.entity.CnAreaEntity;
import com.npwh.member.service.CnAreaService;
import com.npwh.common.utils.PageUtils;
import com.npwh.common.utils.R;



/**
 *  县级（区县）
 *
 * @author Kings
 * @email cqzhongxc@gmail.com
 * @date 2020-09-23 16:55:57
 */
@RestController
@RequestMapping("member/cnarea")
public class CnAreaController {
    @Autowired
    private CnAreaService cnAreaService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    // @RequiresPermissions("member:cnarea:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = cnAreaService.queryPage(params);

        return R.ok().put("page", page);
    }

    @RequestMapping("/city/{code}")
    public R selectByCityCode(@PathVariable("code") String code) {
        List<CnAreaEntity> list = cnAreaService.selectByCityCode(code);
        return R.ok().put("data", list);
    }

    /**
     * 信息
     */
    @RequestMapping("/info/{areaid}")
    // @RequiresPermissions("member:cnarea:info")
    public R info(@PathVariable("areaid") Integer areaid){
		CnAreaEntity cnArea = cnAreaService.getById(areaid);

        return R.ok().put("cnArea", cnArea);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    // @RequiresPermissions("member:cnarea:save")
    public R save(@RequestBody CnAreaEntity cnArea){
		cnAreaService.save(cnArea);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    // @RequiresPermissions("member:cnarea:update")
    public R update(@RequestBody CnAreaEntity cnArea){
		cnAreaService.updateById(cnArea);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    // @RequiresPermissions("member:cnarea:delete")
    public R delete(@RequestBody Integer[] areaids){
		cnAreaService.removeByIds(Arrays.asList(areaids));

        return R.ok();
    }

}
