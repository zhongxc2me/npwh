package com.npwh.member.controller;

import java.util.Arrays;
import java.util.Map;

// import org.apache.shiro.authz.annotation.RequiresPermissions;
import com.npwh.member.service.CardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.npwh.member.entity.OccupySeatEntity;
import com.npwh.member.service.OccupySeatService;
import com.npwh.common.utils.PageUtils;
import com.npwh.common.utils.R;


/**
 * 剧场座位号发放
 *
 * @author Kings
 * @email cqzhongxc@gmail.com
 * @date 2020-10-30 22:31:07
 */
@RestController
@RequestMapping("member/occupyseat")
public class OccupySeatController {
    @Autowired
    private OccupySeatService occupySeatService;

    @Autowired
    private CardService cardService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    // @RequiresPermissions("member:occupyseat:list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = occupySeatService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    // @RequiresPermissions("member:occupyseat:info")
    public R info(@PathVariable("id") Integer id) {
        OccupySeatEntity occupySeat = occupySeatService.getById(id);

        return R.ok().put("occupySeat", occupySeat);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    // @RequiresPermissions("member:occupyseat:save")
    public R save(@RequestBody OccupySeatEntity occupySeat) {
        occupySeatService.save(occupySeat);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    // @RequiresPermissions("member:occupyseat:update")
    public R update(@RequestBody OccupySeatEntity occupySeat) {
        occupySeatService.updateById(occupySeat);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    // @RequiresPermissions("member:occupyseat:delete")
    public R delete(@RequestBody Integer[] ids) {
        occupySeatService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

    /**
     * 获取戏剧座位号
     *
     * @return
     */
    @RequestMapping(value = "/seat", method = RequestMethod.POST)
    public R getSeatNo(@RequestBody Map<String, Object> param) {

        /**
         * 1 - 60 为固定会员座位号
         * 61 - 150 为公开座位号
         *
         * 1. 参数 戏剧编号，会员卡号
         * 2. 按会员卡号查询是否已指定了座位号，有则直接返回，反之往下流程
         * 3. 按戏剧编号查询已发放的座位号，以及会员在后台已指定了的座位号码
         * 4. 排除已发放的座位号，然后先后顺序发放 60 - 150 座位号
         * 5. 记录到数据库
         *
         */

        // 戏剧编号
        String dramaId = param.get("dramaId") + "";
        // 会员卡号
        String cardNumber = param.get("cardNumber") + "";
        // 获取座位号
        int seatNo = occupySeatService.getSeatNo(dramaId, cardNumber);
        // 返回座位号
        return R.ok().put("data", seatNo);
    }

}
