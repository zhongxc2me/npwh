package com.npwh.member.controller;

import java.util.Arrays;
import java.util.Map;

// import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.npwh.member.entity.HandleMethodEntity;
import com.npwh.member.service.HandleMethodService;
import com.npwh.common.utils.PageUtils;
import com.npwh.common.utils.R;



/**
 * 办理方式表 
 *
 * @author Kings
 * @email cqzhongxc@gmail.com
 * @date 2020-09-23 10:38:08
 */
@RestController
@RequestMapping("member/handlemethod")
public class HandleMethodController {
    @Autowired
    private HandleMethodService handleMethodService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    // @RequiresPermissions("member:handlemethod:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = handleMethodService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    // @RequiresPermissions("member:handlemethod:info")
    public R info(@PathVariable("id") Integer id){
		HandleMethodEntity handleMethod = handleMethodService.getById(id);

        return R.ok().put("handleMethod", handleMethod);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    // @RequiresPermissions("member:handlemethod:save")
    public R save(@RequestBody HandleMethodEntity handleMethod){
		handleMethodService.save(handleMethod);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    // @RequiresPermissions("member:handlemethod:update")
    public R update(@RequestBody HandleMethodEntity handleMethod){
		handleMethodService.updateById(handleMethod);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    // @RequiresPermissions("member:handlemethod:delete")
    public R delete(@RequestBody Integer[] ids){
		handleMethodService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
