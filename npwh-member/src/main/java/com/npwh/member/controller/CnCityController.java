package com.npwh.member.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

// import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.npwh.member.entity.CnCityEntity;
import com.npwh.member.service.CnCityService;
import com.npwh.common.utils.PageUtils;
import com.npwh.common.utils.R;



/**
 * 地级（城市）
 *
 * @author Kings
 * @email cqzhongxc@gmail.com
 * @date 2020-09-23 16:55:58
 */
@RestController
@RequestMapping("member/cncity")
public class CnCityController {
    @Autowired
    private CnCityService cnCityService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    // @RequiresPermissions("member:cncity:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = cnCityService.queryPage(params);

        return R.ok().put("page", page);
    }

    @RequestMapping("/province/{code}")
    public R selectByProvinceCode(@PathVariable("code") String code) {
        List<CnCityEntity> list = cnCityService.selectByProvinceCode(code);
        return R.ok().put("data", list);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{cid}")
    // @RequiresPermissions("member:cncity:info")
    public R info(@PathVariable("cid") Integer cid){
		CnCityEntity cnCity = cnCityService.getById(cid);

        return R.ok().put("cnCity", cnCity);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    // @RequiresPermissions("member:cncity:save")
    public R save(@RequestBody CnCityEntity cnCity){
		cnCityService.save(cnCity);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    // @RequiresPermissions("member:cncity:update")
    public R update(@RequestBody CnCityEntity cnCity){
		cnCityService.updateById(cnCity);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    // @RequiresPermissions("member:cncity:delete")
    public R delete(@RequestBody Integer[] cids){
		cnCityService.removeByIds(Arrays.asList(cids));

        return R.ok();
    }

}
